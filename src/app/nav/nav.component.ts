import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toRegister(){
    this.router.navigate(['']);

  }
  toCodes(){
    this.router.navigate(['/Help']);
    
  }
  
  constructor(private router:Router) { }

  ngOnInit() {
  }

}
